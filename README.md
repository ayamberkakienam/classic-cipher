This work is part of IF4020 Cryptography course assignment.
In here contained several classic cryptographic cipher:
- Vignere
- Full Vignere
- Auto-key Vignere
- Running-key Vignere
- Playfair 