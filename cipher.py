import operator as op
import random
import string

def is_char(char, option='uppercase'):
	char_val = ord(char)
	if (option == 'uppercase'):
		return (64 < char_val < 91)
	elif (option == 'lowercase'):
		return (96 < char_val < 122)
	elif (option == 'all_case'):
		return (64 < char_val < 91 or 96 < char_val < 122)
	elif (option == 'all'):
		return (0 < char_val < 255)

def remove_punctuation(text):
	translator = str.maketrans('', '', string.punctuation)
	new_text = text.translate(translator)
	return new_text

def remove_whitespace(text):
	translator = str.maketrans('', '', string.whitespace)
	new_text = text.translate(translator)
	return new_text

def remove_chars(text, chars):
	translator = str.maketrans('', '', chars)
	new_text = text.translate(translator)
	return new_text

def chunk_text(text, size):
	text = remove_whitespace(text)
	text = remove_punctuation(text)
	new_text = ' '.join([text[i:i+size] for i in range(0, len(text), size)])
	return new_text

class Vignere:

	def __init__(self, padding_mode='uppercase', charnum=26):
		self.charnum = 26
		self.padding_mode = padding_mode

		if (padding_mode == 'uppercase'):
			self.padding = 65
		else:
			self.padding = 0

	def preprocess(self, texts):
		new_texts = []
		for text in texts:
			if (self.padding_mode == 'uppercase'):
				text = text.upper()
				new_texts.append(text)
		return new_texts

	def substitute_char(self, pchar, kchar, forward=True):
		pchar = ord(pchar) - self.padding
		kchar = ord(kchar) - self.padding

		cchar = None
		if (forward):
			cchar = pchar + kchar
		else:
			cchar = pchar - kchar

		cchar = op.mod(cchar, self.charnum) + self.padding

		return chr(cchar)

	def encrypt(self, ptext, key):
		ptext, key = self.preprocess([ptext, key])

		ctext = ''
		len_ptext = len(ptext)
		len_key = len(key)

		key_idx = 0
		for cchar in ptext:
			if (is_char(cchar)):
				j = op.mod(key_idx, len_key)
				cchar = self.substitute_char(cchar, key[j])
				key_idx += 1
			ctext += cchar
		return ctext

	def decrypt(self, ctext, key):
		ctext, key = self.preprocess([ctext, key])

		ptext = ''
		len_ctext = len(ctext)
		len_key = len(key)

		key_idx = 0
		for pchar in ctext:
			if (is_char(pchar)):
				j = op.mod(key_idx, len_key)
				pchar = self.substitute_char(pchar, key[j], forward=False)
				key_idx += 1
			ptext += pchar
		return ptext		

class VignereFull:

	def __init__(self, seed='jekk', padding_mode='uppercase', charnum=26):
		self.charnum = charnum
		self.seed = seed
		self.padding_mode = padding_mode
		if (padding_mode == 'uppercase'):
			self.padding = 65
		else:
			self.padding = 0
		self.matrice = self.generate_matrice()

	def preprocess(self, texts):
		new_texts = []
		for text in texts:
			if (self.padding_mode == 'uppercase'):
				text = text.upper()
				new_texts.append(text)
		return new_texts

	def generate_matrice(self):
		random.seed(self.seed)

		chars = list(map(chr, range(self.padding, self.padding + self.charnum)))
		random.shuffle(chars)

		vignere_matrice = []
		for i in range(0, self.charnum):
			vignere_matrice.append(chars[i:] + chars[:i])
		
		random.shuffle(vignere_matrice)

		return vignere_matrice

	def encrypt(self, ptext, key):
		ptext, key = self.preprocess([ptext, key])

		ctext = ''
		len_ptext = len(ptext)
		len_key = len(key)

		key_idx = 0
		for cchar in ptext:
			if (is_char(cchar)):
				j = op.mod(key_idx, len_key)
				x = ord(key[j]) - self.padding
				y = ord(cchar) - self.padding
				cchar = self.matrice[x][y]
				key_idx += 1
			ctext += cchar
		return ctext

	def decrypt(self, ctext, key):
		ctext, key = self.preprocess([ctext, key])

		ptext = ''
		len_ctext = len(ctext)
		len_key = len(key)

		key_idx = 0
		for pchar in ctext:
			if (is_char(pchar)):
				j = op.mod(key_idx, len_key)
				x = ord(key[j]) - self.padding
				pchar = self.matrice[x].index(pchar)
				pchar = chr(pchar + self.padding)
				key_idx += 1
			ptext += pchar
		return ptext

class VignereAuto:

	def __init__(self, padding_mode='uppercase', charnum=26):
		self.charnum = 26
		self.padding_mode = padding_mode

		if (padding_mode == 'uppercase'):
			self.padding = 65
		else:
			self.padding = 0

	def preprocess(self, texts):
		new_texts = []
		for text in texts:
			if (self.padding_mode == 'uppercase'):
				text = text.upper()
				new_texts.append(text)
		return new_texts

	def substitute_char(self, pchar, kchar, forward=True):
		pchar = ord(pchar) - self.padding
		kchar = ord(kchar) - self.padding

		cchar = None
		if (forward):
			cchar = pchar + kchar
		else:
			cchar = pchar - kchar

		cchar = op.mod(cchar, self.charnum) + self.padding

		return chr(cchar)

	def adjust_key(self, ptext, key):
		if len(ptext) > len(key):
			ptext = remove_punctuation(ptext)
			ptext = remove_whitespace(ptext)
			text_padding = len(ptext) - len(key)
			return key + ptext[:text_padding]
		else:
			return key

	def encrypt(self, ptext, key):
		ptext, key = self.preprocess([ptext, key])
		key = self.adjust_key(ptext, key)
		print('key: ', key)

		ctext = ''
		len_ptext = len(ptext)

		key_idx = 0
		for cchar in ptext:
			if (is_char(cchar)):
				cchar = self.substitute_char(cchar, key[key_idx])
				key_idx += 1
			ctext += cchar
		return ctext

	def decrypt(self, ctext, key):
		ctext, key = self.preprocess([ctext, key])

		ptext = ''
		len_ctext = len(ctext)
		len_key = len(key)

		key_idx = 0
		rkey_idx = 0
		for pchar in ctext:
			if (is_char(pchar)):
				if (key_idx < len_key):
					pchar = self.substitute_char(pchar, key[key_idx], forward=False)
					key_idx += 1
				else:
					while not is_char(ptext[rkey_idx]):
						rkey_idx += 1
					pchar = self.substitute_char(pchar, ptext[rkey_idx], forward=False)
					rkey_idx += 1
			ptext += pchar
		return ptext		

class VignereRunning:

	def __init__(self, padding_mode='uppercase', charnum=26):
		self.charnum = 26
		self.padding_mode = padding_mode

		if (padding_mode == 'uppercase'):
			self.padding = 65
		else:
			self.padding = 0

	def load_key(self, filename, text_size):
		key = ''
		with open(filename) as keyfile:
			while (len(key) < text_size):
				c = keyfile.read(1)
				if is_char(c, option='all_case'):
					key += c
		return key				

	def preprocess(self, texts, mode='uppercase'):
		new_texts = []

		for text in texts:
			if (mode == 'uppercase'):
				text = text.upper()
				new_texts.append(text)
		return new_texts

	def substitute_char(self, pchar, kchar, forward=True):
		pchar = ord(pchar) - self.padding
		kchar = ord(kchar) - self.padding

		cchar = None
		if (forward):
			cchar = pchar + kchar
		else:
			cchar = pchar - kchar

		cchar = op.mod(cchar, self.charnum) + self.padding

		return chr(cchar)

	def encrypt(self, ptext, keyfile):
		key = self.load_key(keyfile, len(ptext))
		ptext, key = self.preprocess([ptext, key])

		ctext = ''
		len_ptext = len(ptext)
		len_key = len(key)

		for i in range (0, len_ptext):
			cchar = ptext[i]
			if (is_char(cchar)):
				j = op.mod(i, len_key)
				cchar = self.substitute_char(cchar, key[j])
				print('SUBS!')
			ctext += cchar
		return ctext

	def decrypt(self, ctext, keyfile):
		key = self.load_key(keyfile, len(ctext))
		ctext, key = self.preprocess([ctext, key])

		ptext = ''
		len_ctext = len(ctext)
		len_key = len(key)

		for i in range (0, len_ctext):
			pchar = ctext[i]
			if (is_char(pchar)):
				j = op.mod(i, len_key)
				pchar = self.substitute_char(pchar, key[j], forward=False)
			ptext += pchar
		return ptext		

class VignereExtended:

	def __init__(self):
		self.charnum = 256
		self.padding_mode = 'all'

	def read_file(self, filename):
		data = None
		with open(filename, 'rb') as file:
			data = file.read()
		return data

	def write_file(self, filename, data):
		with open(filename, 'wb') as file:
			file.write(data)

	def preprocess(self, texts):
		new_texts = []
		for text in texts:
			if (self.padding_mode == 'all'):
				text = str.encode(text)
			new_texts.append(text)
		return new_texts

	def substitute_char(self, pchar, kchar, forward=True):
		pchar = pchar
		kchar = kchar

		cchar = None
		if (forward):
			cchar = pchar + kchar
		else:
			cchar = pchar - kchar

		cchar = op.mod(cchar, self.charnum)

		return cchar

	def encrypt(self, filename, key):
		cdata = bytearray()
		
		pdata = self.read_file(filename)
		key = self.preprocess([key])[0]

		len_pdata = len(pdata)
		len_key = len(key)

		key_idx = 0
		for cchar in pdata:
			j = op.mod(key_idx, len_key)
			cchar = self.substitute_char(cchar, key[j])
			key_idx += 1
			cdata.append(cchar)
		
		# cdata = str.encode(cdata)
		self.write_file(filename, cdata)

	def decrypt(self, filename, key):
		pdata = bytearray()

		cdata = self.read_file(filename)
		key = self.preprocess([key])[0]

		len_cdata = len(cdata)
		len_key = len(key)

		key_idx = 0
		for pchar in cdata:
			j = op.mod(key_idx, len_key)
			pchar = self.substitute_char(pchar, key[j], forward=False)
			key_idx += 1
			pdata.append(pchar)

		# pdata = str.encode(pdata)
		self.write_file(filename, pdata)

class Playfair:

	def __init__(self, bad_char='J', good_char='I', dex_char='X', padding_mode='uppercase'):
		self.padding_mode = padding_mode
		self.bad_char, self.good_char, self.dex_char = self.preprocess([bad_char, good_char, dex_char])
		self.matrice_size = 5

	def preprocess(self, texts):
		new_texts = []
		for text in texts:
			if (self.padding_mode == 'uppercase'):
				text = text.upper()
				new_texts.append(text)
		return new_texts

	def generate_chars(self):
		if (self.padding_mode == 'uppercase'):
			return string.ascii_uppercase

	def adjust_key(self, key):
		key = remove_whitespace(key)
		key = ''.join(sorted(set(key), key=key.index))
		tail_key = remove_chars(self.generate_chars(), key+self.bad_char) 
		return key+tail_key

	def adjust_ptext(self, ptext):
		ptext = remove_whitespace(ptext)
		ptext = remove_punctuation(ptext)
		ptext = ptext.replace(self.bad_char, self.good_char)
		for i in range(0, len(ptext)):
			if (ptext[i] == ptext[i+1]):
				ptext = ptext[:i+1] + self.dex_char + ptext[i+1:]
		if len(ptext) % 2 != 0:
			ptext += self.dex_char
		return ptext

	def generate_matrice(self, key):
		key = self.adjust_key(key)
		matrice = []
		key_idx = 0
		for _ in range(0, self.matrice_size):
			row = []
			for _ in range(0, self.matrice_size):
				row.append(key[key_idx])
				key_idx += 1
			matrice.append(row)
		return matrice

	def locate_char(self, char, matrice):
		i = 0
		while char not in matrice[i]:
			i += 1
		return i, matrice[i].index(char)

	def encrypt(self, ptext, key):
		ptext, key = self.preprocess([ptext, key])
		matrice = self.generate_matrice(key)

		for row in matrice:
			print(row)

		clean_ptext = self.adjust_ptext(ptext)
		print('clean_ptext: ', clean_ptext)
		clean_ctext = ''

		i = 0
		while i < len(clean_ptext):
			char_a = {
				'char': clean_ptext[i],
				'x': -1,
				'y': -1
			}
			char_b = {
				'char': clean_ptext[i+1],
				'x': -1,
				'y': -1
			}
			
			char_a['x'], char_a['y'] = self.locate_char(char_a['char'], matrice)
			char_b['x'], char_b['y'] = self.locate_char(char_b['char'], matrice)

			if char_a['x'] == char_b['x']:
				cchar_a = matrice[(char_a['x']+1)%5][char_a['y']]
				cchar_b = matrice[(char_b['x']+1)%5][char_b['y']]
			elif char_a['y'] == char_b['y']:
				cchar_a = matrice[char_a['x']][(char_a['y']+1)%5]
				cchar_b = matrice[char_b['x']][(char_b['y']+1)%5]
			else:
				cchar_a = matrice[char_a['x']][char_b['y']]
				cchar_b = matrice[char_b['x']][char_a['y']]
			
			clean_ctext += cchar_a + cchar_b
			i += 2
		return clean_ctext

	# RIGHT NOW, ASSUME CIPHERTEXT ALREADY CLEAN
	def decrypt(self, ctext, key):
		ctext, key = self.preprocess([ctext, key])
		matrice = self.generate_matrice(key)

		clean_ptext = ''

		i = 0
		while i < len(ctext):
			char_a = {
				'char': ctext[i],
				'x': -1,
				'y': -1
			}
			char_b = {
				'char': ctext[i+1],
				'x': -1,
				'y': -1
			}

			char_a['x'], char_a['y'] = self.locate_char(char_a['char'], matrice)
			char_b['x'], char_b['y'] = self.locate_char(char_b['char'], matrice)

			if char_a['x'] == char_b['x']:
				cchar_a = matrice[char_a['x']-1][char_a['y']]
				cchar_b = matrice[char_b['x']-1][char_b['y']]
			elif char_a['y'] == char_b['y']:
				cchar_a = matrice[char_a['x']][char_a['y']-1]
				cchar_b = matrice[char_b['x']][char_b['y']-1]
			else:
				cchar_a = matrice[char_a['x']][char_b['y']]
				cchar_b = matrice[char_b['x']][char_a['y']]

			clean_ptext += cchar_a + cchar_b
			i += 2
		return clean_ptext

class Menu:
	def __init__(self):
		pass

	def print_gray(self, text):
		text = '\033[1;30m' + text + '\033[1;m'
		print(text)

	def print_red(self, text):
		text = '\033[1;31m' + text + '\033[1;m'
		print(text)

	def print_green(self, text):
		text = '\033[1;32m' + text + '\033[1;m'
		print(text)

	def print_yellow(self, text):
		text = '\033[1;33m' + text + '\033[1;m'
		print(text)

	def print_blue(self, text):
		text = '\033[1;34m' + text + '\033[1;m'
		print(text)

	def print_magenta(self, text):
		text = '\033[1;35m' + text + '\033[1;m'
		print(text)

	def print_cyan(self, text):
		text = '\033[1;36m' + text + '\033[1;m'
		print(text)

	def print_white(self, text):
		text = '\033[1;37m' + text + '\033[1;m'
		print(text)

	def print_crimson(self, text):
		text = '\033[1;38m' + text + '\033[1;m'
		print(text)

	def print_highlighted_red(self, text):
		text = '\033[1;41m' + text + '\033[1;m'
		print(text)

	def print_highlighted_green(self, text):
		text = '\033[1;42m' + text + '\033[1;m'
		print(text)

	def print_highlighted_brown(self, text):
		text = '\033[1;43m' + text + '\033[1;m'
		print(text)

	def print_highlighted_blue(self, text):
		text = '\033[1;44m' + text + '\033[1;m'
		print(text)

	def print_highlighted_magenta(self, text):
		text = '\033[1;45m' + text + '\033[1;m'
		print(text)

	def print_highlighted_cyan(self, text):
		text = '\033[1;46m' + text + '\033[1;m'
		print(text)

	def print_highlighted_gray(self, text):
		text = '\033[1;47m' + text + '\033[1;m'
		print(text)

	def print_highlighted_crimson(self, text):
		text = '\033[1;48m' + text + '\033[1;m'
		print(text)

	def separator(self, text):
		print()
		self.print_cyan('-============== ' + text + ' ==============-')
		print()

	def validate_input(self, inp, valid_range):
		return inp in valid_range

	def show_cipher_selection_keyboard(self):
		
		def _show():
			print()
			self.print_green('- Choose cipher -')
			self.print_highlighted_blue('* Extended Vigènere can only generate plain cipher.')
			self.print_highlighted_blue('  Input and result won\'t be printed!')
			self.print_highlighted_blue('* Running-Key Vigènere requires key file name!')
			self.print_magenta('[1] Standard Vigènere\t[4] Running-Key Vigènere')
			self.print_magenta('[2] Full Vigènere\t[5] Extended Vigènere (ASCII encoding)')
			self.print_magenta('[3] Auto-Key Vigènere\t[6] Playfair')
			print()

		valid_range = ['1', '2', '3', '4', '5', '6']

		_show()
		inp = input('Enter choice: ')
		while not self.validate_input(inp, valid_range):
			self.print_highlighted_red('Invalid choice!')
			_show()
			inp = input('Enter choice: ')
		return inp

	def show_cipher_selection_file(self):
		
		def _show():
			print()
			self.print_green('- Choose cipher -')
			self.print_magenta('[5] Extended Vigènere (ASCII encoding)')
			print()

		valid_range = ['5']

		_show()
		inp = input('Enter choice: ')
		while not self.validate_input(inp, valid_range):
			self.print_highlighted_red('Invalid choice!')
			_show()
			inp = input('Enter choice: ')
		return inp

	def show_operation_selection(self):

		def _show():
			print()
			self.print_green('- What do you want to do? -')
			self.print_magenta('[1] Encrypt stuff\t[2] Decrypt stuff')
			print()

		valid_range = ['1', '2']

		_show()
		inp = input('Enter choice: ')
		while not self.validate_input(inp, valid_range):
			self.print_highlighted_red('Invalid choice!')
			_show()
			inp = input('Enter choice: ')
		return inp

	def show_input_selection(self):

		def _show():
			print()
			self.print_green('- What kind of stuff? -')
			self.print_magenta('[1] Keyboard input\t[2] Text file')
			self.print_magenta('[3] Other file')
			print()

		valid_range = ['1', '2', '3']

		_show()
		inp = input('Enter choice: ')
		while not self.validate_input(inp, valid_range):
			self.print_highlighted_red('Invalid choice!')
			_show()
			inp = input('Enter choice: ')
		return inp

	def show_output_selection(self):

		def _show():
			print()
			self.print_green('- How the ciphertext should be displayed? -')
			self.print_magenta('[1] Plain (preserve whitespace and punctuation)')
			self.print_magenta('[2] Without whitespace (preserve punctuation)')
			self.print_magenta('[3] Group-of-five (letters only)')
			print()

		valid_range = ['1', '2', '3']

		_show()
		inp = input('Enter choice: ')
		while not self.validate_input(inp, valid_range):
			self.print_highlighted_red('Invalid choice!')
			_show()
			inp = input('Enter choice: ')
		return inp

	def show_closing(self):
		self.print_highlighted_blue('File operation result will overwrite the file')
		self.print_highlighted_blue('Output will be stored in "Cipher_Output" file')

	def show_text(self, title, content):
		print()
		print(title + ':')
		print(content)
		print()

	def input_text(self):
		inp = input('Input text: ')
		return inp

	def input_filename(self):
		inp = input('Input file name: ')
		return inp

	def input_key(self):
		inp = input('Input key: ')
		return inp

if __name__ == '__main__':

	menu = Menu()

	# [1] Encryption
	# [2] Decryption
	menu_op = menu.show_operation_selection()
	
	# [1] Keyboard
	# [2] File text
	# [3] File other
	menu_inp_type = menu.show_input_selection()

	if menu_inp_type == '1' or menu_inp_type == '2':
		# [1] Standard Vigènere		[4] Running-Key Vigènere
		# [2] Full Vigènere				[5] Extended Vigènere (ASCII encoding)
		# [3] Auto-Key Vigènere		[6] Playfair
		menu_cipher = menu.show_cipher_selection_keyboard()
	elif menu_inp_type == '3':
		# [5] Extended Vigènere (ASCII encoding)
		menu_cipher = menu.show_cipher_selection_file()

	if menu_cipher == '1':
		cipher = Vignere()
	elif menu_cipher == '2':
		cipher = VignereFull()
	elif menu_cipher == '3':
		cipher = VignereAuto()
	elif menu_cipher == '4':
		cipher = VignereRunning()
	elif menu_cipher == '5':
		cipher = VignereExtended()
	elif menu_cipher == '6':
		cipher = Playfair()

	# -============== *** ==============- # 
	
	menu.separator('Gotcha!')

	key = menu.input_key()
	
	if menu_inp_type == '1':
		input_text = menu.input_text()
		if menu_cipher == '5':		# Special care for Extended Vignere
			with open('Cipher_Output', 'w') as file:
				file.write(input_text)
			input_text = 'Cipher_Output'
	elif menu_inp_type == '2':
		input_text = menu.input_filename()
		if menu_cipher in ['1', '2', '3', '4', '6']:
			with open(input_text, 'r') as file:
				input_text = file.read()
	elif menu_inp_type == '3':
		input_text = menu.input_filename()

	if menu_op == '1':
		output = cipher.encrypt(input_text, key)
	elif menu_op == '2':
		output = cipher.decrypt(input_text, key)

	# -============== *** ==============- # 

	menu.separator('Operation Succeed!')

	if menu_cipher != '5':
		# [1] Plain
		# [2] Without whitespace
		# [3] Group-of-five
		menu_out_type = menu.show_output_selection()

		if menu_out_type == '1':
			pass
		elif menu_out_type == '2':
			output = remove_whitespace(output)
		elif menu_out_type == '3':
			output = chunk_text(output, 5)

		with open('Cipher_Output', 'w') as file:
			file.write(output)
		menu.show_text('Input', input_text)
		menu.show_text('Output', output)

	menu.show_closing()